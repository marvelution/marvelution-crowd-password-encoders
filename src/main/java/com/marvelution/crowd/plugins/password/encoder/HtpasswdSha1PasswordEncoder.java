/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.crowd.plugins.password.encoder;

import java.security.MessageDigest;

import org.apache.log4j.Logger;

import sun.misc.BASE64Encoder;

import com.atlassian.crowd.exception.PasswordEncoderException;
import com.atlassian.crowd.password.encoder.InternalPasswordEncoder;

/**
 * {@link InternalPasswordEncoder} to enable support for Apaches' htpasswd SHA-1 Password encryption
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
@SuppressWarnings("restriction")
public class HtpasswdSha1PasswordEncoder implements InternalPasswordEncoder {
	
	private final Logger logger = Logger.getLogger(HtpasswdSha1PasswordEncoder.class);

	private boolean forceLowerCasePrefix;
	
	public static final String APACHE_HTPASSWD_KEY = "apache-htpasswd-sha1";
	
	public static final String SHA_PREFIX = "{SHA1}";
	
	public static final String SHA_PREFIX_LC = SHA_PREFIX.toLowerCase();
	
	/**
	 * Default Constructor
	 */
	public HtpasswdSha1PasswordEncoder() {
		forceLowerCasePrefix = false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public String encodePassword(String rawPass, Object salt) {
		try {
			final BASE64Encoder encoder = new BASE64Encoder();
			final MessageDigest digest = MessageDigest.getInstance("SHA1");
			final byte[] digPass = digest.digest(rawPass.getBytes());
			final String encPass = encoder.encode(digPass);
			final String prefix = (forceLowerCasePrefix ? SHA_PREFIX_LC : SHA_PREFIX);
			return prefix + encPass;
		} catch (Exception e) {
			logger.error("Failed to encrpyt password to APACHE-HTPASSWD-SHA1. Reason: " + e.getMessage(), e);
			throw new PasswordEncoderException("Failed to encrpyt password to APACHE-HTPASSWD-SHA1", e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isPasswordValid(String encPass, String rawPass, Object salt) {
		boolean valid = false;
		final boolean forceLowercase = forceLowerCasePrefix;
		forceLowerCasePrefix = false;
		valid = encodePassword(rawPass, salt).equals(encPass);
		forceLowerCasePrefix = forceLowercase;
		return valid;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getKey() {
		return APACHE_HTPASSWD_KEY;
	}
	
	/**
	 * Sets whether the encrypted password should have a Upper- or Lowercase prefix
	 * 
	 * @param forceLowerCasePrefix <code>true</code> for Lowercase prefix, <code>false</code> for Uppercase
	 */
	public void setForceLowerCasePrefix(boolean forceLowerCasePrefix) {
		this.forceLowerCasePrefix = forceLowerCasePrefix;
	}

}
