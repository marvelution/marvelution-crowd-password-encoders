/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.crowd.plugins.password.encoder;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * TestCase for {@link HtpasswdSha1PasswordEncoder}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class HtpasswdSha1PasswordEncoderTest {

	/**
	 * Test password encoding
	 */
	@Test
	public void testEncodePassword() {
		final HtpasswdSha1PasswordEncoder encoder = new HtpasswdSha1PasswordEncoder();
		assertEquals("{SHA1}3HJK8Y+91OWRifX+dopfgxFScFA=", encoder.encodePassword("testing", null));
	}

	/**
	 * Test if password is valid
	 */
	@Test
	public void testValidPassword() {
		final HtpasswdSha1PasswordEncoder encoder = new HtpasswdSha1PasswordEncoder();
		assertTrue(encoder.isPasswordValid("{SHA1}3HJK8Y+91OWRifX+dopfgxFScFA=", "testing", null));
	}

	/**
	 * Test if password is invalid
	 */
	@Test
	public void testInvalidPassword() {
		final HtpasswdSha1PasswordEncoder encoder = new HtpasswdSha1PasswordEncoder();
		assertFalse(encoder.isPasswordValid("{SHA1}3HJK8Y+91OWRifX+dopfgxFScFA=", "invalid", null));
	}

	/**
	 * Test getKey
	 */
	@Test
	public void testGetKey() {
		final HtpasswdSha1PasswordEncoder encoder = new HtpasswdSha1PasswordEncoder();
		assertEquals("apache-htpasswd-sha1", encoder.getKey());
	}

	/**
	 * Test lowercase prefix
	 */
	@Test
	public void testSetForceLowerCasePrefix() {
		final HtpasswdSha1PasswordEncoder encoder = new HtpasswdSha1PasswordEncoder();
		encoder.setForceLowerCasePrefix(true);
		assertTrue(encoder.encodePassword("testing", null).startsWith(HtpasswdSha1PasswordEncoder.SHA_PREFIX_LC));
	}

}
